/* This script handle the motion simulator
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using UnityEngine.Analytics;

public class DeLorean : MonoBehaviour
{
    Dictionary<string, string> config = new Dictionary<string, string>();
    // The 2 telemetry files were produced with Video Ride Creator. The first one is used for the 3DOF motion simulator
    // It can be read by the video ride player
    // The 2nd one is used for the fan (speed)
    private int nb_lines = 0, time_start_car;
    private float update;
    private bool run_thread = true;
    private int loop_ms;
    private int cpt_line = -1, old_cpt_line = -1;
    private System.Threading.Thread telemetry_thread;
    private string[] metrics;
    public GameObject de_lorean, parent;
    private float[,] de_lorean_rotation;
    IPEndPoint remote_end_point;
    static UdpClient client;

    public void Start()
    {
        System.IO.StreamReader file = new System.IO.StreamReader(@"settings.cfg");
        string line = "";
        while ((line = file.ReadLine()) != null)
        {
            string[] items = line.Split('=');
            config[items[0].Trim()] = items[1].Trim();
        }
        file.Close();
        de_lorean.transform.parent = parent.transform;
        remote_end_point = new IPEndPoint(IPAddress.Parse(config["ip"]), Int32.Parse(config["port"]));
        client = new UdpClient();
        string[] lines_main = System.IO.File.ReadAllLines(@config["file_telemetry_main"]);
        string[] lines_dash = System.IO.File.ReadAllLines(config["file_telemetry_dashboard"]);
        metrics = new string[lines_main.Length];
        de_lorean_rotation = new float[lines_main.Length, 2];
        string sep = "\t";
        // The biggest part of the work must be done here as we want to have the best performances
        // in the code which is performing the motions
        while (nb_lines < lines_main.Length)
        {
            string[] words_main = lines_main[nb_lines].Split(sep.ToCharArray());
            string[] words_dash = lines_dash[nb_lines].Split(sep.ToCharArray());
            double speed = double.Parse(words_dash[0].Replace(",", "."));
            if (speed < 0) speed = 0;
            speed = (speed / float.Parse(config["val_max"])) * 141f; // 141 kmh == 88 miles per hour (max speed) :)
            // Send Telemetry.
            metrics[nb_lines] = String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}",
                                           (int)double.Parse(words_main[0].Replace(",", ".")),
                                           (int)double.Parse(words_main[1].Replace(",", ".")),
                                           (int)double.Parse(words_main[2].Replace(",", ".")),
                                           (int)double.Parse(words_main[3].Replace(",", ".")),
                                           (int)double.Parse(words_main[4].Replace(",", ".")),
                                           (int)double.Parse(words_main[5].Replace(",", ".")),
                                           0,
                                           0,
                                           (int)speed);
            for (int i = 0; i < 2; ++i)
                de_lorean_rotation[nb_lines, i] = Convert.ToSingle(double.Parse(words_main[i].Replace(",", ".")) / float.Parse(config["val_max"]) * Int32.Parse(config["max_degree"]));
            nb_lines++;
        }
        time_start_car = 5 * Convert.ToInt16(config["nb_points_per_sec"]); // s
        loop_ms =  (int)(1.0f / Convert.ToInt16(config["nb_points_per_sec"]) * 1000.0f); // 25 points per sec
        telemetry_thread = new System.Threading.Thread(Send_telemetry);
        telemetry_thread.IsBackground = true;
        telemetry_thread.Start();
        cpt_line = 0;
    }

    // Update is called once per frame
    public void Update()
    {
        Transform par = parent.transform;
        update += Time.deltaTime;
        if (update >= loop_ms / 1000f)
        {
            update -= loop_ms / 1000f;
            // This part must be as simple as possible and should not take more than 40 ms to be executed
            // Otherwhise telemetry data may be ignored!
            if (cpt_line >= 0)
            {
                if (cpt_line < metrics.Length)
                {
                    // Moving up the car
                    if (par.transform.position.y < 4 && cpt_line > time_start_car)
                        par.transform.position += par.transform.up * Time.deltaTime;
                   // Those lines move the virtual DeLorean with the motion simulator.
                   // I disable that since it makes me sick with VR :)
                   // de_lorean.transform.rotation = Quaternion.identity;
                   // de_lorean.transform.RotateAround(par.position, par.right, -de_lorean_rotation[cpt_line, 1]);
                   // de_lorean.transform.RotateAround(par.position, par.forward, de_lorean_rotation[cpt_line, 0]);
                    cpt_line++;
                }
                else
                {
                    // Moving down the car
                    if (par.transform.position.y > 0.546)
                        par.transform.position -= par.transform.up * Time.deltaTime;
                }
            }
        }
    }

    private void OnDestroy()
    {
        run_thread = false;
    }

    private void Send_telemetry()
    {
        // This part should also be as fast as possible to not loose telemetry data
        while (run_thread)
        {
            if (cpt_line != old_cpt_line && cpt_line < metrics.Length)
            {
                old_cpt_line = cpt_line;
                byte[] data = Encoding.Default.GetBytes(metrics[cpt_line]);
                client.Send(data, data.Length, remote_end_point);
            }
        }
    }
}