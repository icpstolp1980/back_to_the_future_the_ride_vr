﻿using System.Collections;
using UnityEngine;

public class PreshowRoom : MonoBehaviour
{
    public GameObject[] lights;

    // Use this for initialization
    public void Start()
    {
        // Recenter the head
        UnityEngine.XR.InputTracking.Recenter();
        UnityEngine.XR.XRDevice.SetTrackingSpaceType(UnityEngine.XR.TrackingSpaceType.Stationary);
        UnityEngine.XR.InputTracking.Recenter();
        lights = GameObject.FindGameObjectsWithTag("light_preshow");
        StartCoroutine(run());
    }

    IEnumerator run()
    {
        yield return new WaitForSeconds(4 * 60);
        for (int i = 0; i < lights.Length; ++i)
        {
            while (lights[i].GetComponent<Light>().intensity > 0)
            {
                lights[i].GetComponent<Light>().intensity -= 0.1f;
                yield return new WaitForSeconds(0.1f);
            }
        }

        (transform.parent.gameObject.GetComponent("Main") as MonoBehaviour).enabled = true;
        Camera camera = GameObject.Find("Camera Main").GetComponent<Camera>();
        camera.enabled = true;
    }
}
 
