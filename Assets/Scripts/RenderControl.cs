﻿/*
 * Used to hide the garage
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderControl : MonoBehaviour
{

    private Renderer[] sub_renderers;

    private void SetSubrenders(bool state)
    {
        foreach (Renderer r in this.sub_renderers)
        {
            r.enabled = state;
        }
    }

    public Renderer[] GetSubRenderers()
    {
        return this.sub_renderers;
    }

    public void Enable()
    {
        this.SetSubrenders(true);
    }

    public void Disable()
    {
        this.SetSubrenders(false);
    }

    void Start()
    {
        this.sub_renderers = this.gameObject.GetComponentsInChildren<Renderer>();
    }
}
