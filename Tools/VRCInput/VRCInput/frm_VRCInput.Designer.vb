﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_VRCInput
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_VRCInput))
        Me.cb_Slider = New System.Windows.Forms.HScrollBar()
        Me.tmr_Send = New System.Windows.Forms.Timer(Me.components)
        Me.btn_Off = New System.Windows.Forms.Button()
        Me.cbo_OutputSelector = New System.Windows.Forms.ComboBox()
        Me.txt_ScrollValue = New System.Windows.Forms.TextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'cb_Slider
        '
        Me.cb_Slider.LargeChange = 1
        Me.cb_Slider.Location = New System.Drawing.Point(335, 12)
        Me.cb_Slider.Maximum = 32767
        Me.cb_Slider.Minimum = -32767
        Me.cb_Slider.Name = "cb_Slider"
        Me.cb_Slider.Size = New System.Drawing.Size(544, 24)
        Me.cb_Slider.TabIndex = 0
        '
        'tmr_Send
        '
        Me.tmr_Send.Interval = 5
        '
        'btn_Off
        '
        Me.btn_Off.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Off.Location = New System.Drawing.Point(11, 11)
        Me.btn_Off.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_Off.Name = "btn_Off"
        Me.btn_Off.Size = New System.Drawing.Size(109, 32)
        Me.btn_Off.TabIndex = 1
        Me.btn_Off.Text = "Off"
        Me.btn_Off.UseVisualStyleBackColor = True
        '
        'cbo_OutputSelector
        '
        Me.cbo_OutputSelector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbo_OutputSelector.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbo_OutputSelector.FormattingEnabled = True
        Me.cbo_OutputSelector.Items.AddRange(New Object() {"All", "Roll", "Pitch", "Heave", "Yaw", "Sway", "Surge"})
        Me.cbo_OutputSelector.Location = New System.Drawing.Point(124, 12)
        Me.cbo_OutputSelector.Margin = New System.Windows.Forms.Padding(4)
        Me.cbo_OutputSelector.Name = "cbo_OutputSelector"
        Me.cbo_OutputSelector.Size = New System.Drawing.Size(205, 28)
        Me.cbo_OutputSelector.TabIndex = 2
        '
        'txt_ScrollValue
        '
        Me.txt_ScrollValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ScrollValue.Location = New System.Drawing.Point(883, 11)
        Me.txt_ScrollValue.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_ScrollValue.Name = "txt_ScrollValue"
        Me.txt_ScrollValue.Size = New System.Drawing.Size(132, 30)
        Me.txt_ScrollValue.TabIndex = 3
        Me.txt_ScrollValue.Text = "0"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 10
        '
        'frm_VRCInput
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ClientSize = New System.Drawing.Size(1028, 49)
        Me.Controls.Add(Me.txt_ScrollValue)
        Me.Controls.Add(Me.cbo_OutputSelector)
        Me.Controls.Add(Me.btn_Off)
        Me.Controls.Add(Me.cb_Slider)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_VRCInput"
        Me.ShowInTaskbar = False
        Me.Text = "VRC Input Example"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cb_Slider As System.Windows.Forms.HScrollBar
    Friend WithEvents btn_Off As Button
    Friend WithEvents cbo_OutputSelector As ComboBox
    Friend WithEvents txt_ScrollValue As TextBox
    Private WithEvents tmr_Send As Timer
    Friend WithEvents Timer1 As Timer
End Class
