﻿Option Strict On
Option Explicit On
Imports SharpDX.XInput
Public Class frm_VRCInput
    'turn output on and off
    Private Output_IsEnabled As Boolean = False
    Private Sub btn_Off_Click(sender As Object, e As EventArgs) Handles btn_Off.Click
        If btn_Off.Text = "Off" Then
            btn_Off.Text = "On"
            Output_IsEnabled = True
            tmr_Send.Enabled = True
        Else
            btn_Off.Text = "Off"
            Output_IsEnabled = False
        End If
    End Sub

    'Start UDP output
    Private Sub VRCInput_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        cbo_OutputSelector.SelectedIndex = 1 'Roll
        StartUDP() 'StartUDP Output
    End Sub

    'Stop UDP output
    Private Sub VRCInput_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        StopUDP()
    End Sub

    'select what to send and send it
    Private Sub tmr_Send_Tick(sender As System.Object, e As System.EventArgs) Handles tmr_Send.Tick
        If Output_IsEnabled = True Then
            Select Case cbo_OutputSelector.Text
                Case "All"
                    MyOutputs.Roll = cb_Slider.Value.ToString
                    MyOutputs.Pitch = cb_Slider.Value.ToString
                    MyOutputs.Heave = cb_Slider.Value.ToString
                    MyOutputs.Yaw = cb_Slider.Value.ToString
                    MyOutputs.Sway = cb_Slider.Value.ToString
                    MyOutputs.Surge = cb_Slider.Value.ToString
                Case "Roll"
                    MyOutputs.Roll = cb_Slider.Value.ToString
                    MyOutputs.Pitch = ""
                    MyOutputs.Heave = ""
                    MyOutputs.Yaw = ""
                    MyOutputs.Sway = ""
                    MyOutputs.Surge = ""
                Case "Pitch"
                    MyOutputs.Roll = ""
                    MyOutputs.Pitch = cb_Slider.Value.ToString
                    MyOutputs.Heave = ""
                    MyOutputs.Yaw = ""
                    MyOutputs.Sway = ""
                    MyOutputs.Surge = ""
                Case "Heave"
                    MyOutputs.Roll = ""
                    MyOutputs.Pitch = ""
                    MyOutputs.Heave = cb_Slider.Value.ToString
                    MyOutputs.Yaw = ""
                    MyOutputs.Sway = ""
                    MyOutputs.Surge = ""
                Case "Yaw"
                    MyOutputs.Roll = ""
                    MyOutputs.Pitch = ""
                    MyOutputs.Heave = ""
                    MyOutputs.Yaw = cb_Slider.Value.ToString
                    MyOutputs.Sway = ""
                    MyOutputs.Surge = ""
                Case "Sway"
                    MyOutputs.Roll = ""
                    MyOutputs.Pitch = ""
                    MyOutputs.Heave = ""
                    MyOutputs.Yaw = ""
                    MyOutputs.Sway = cb_Slider.Value.ToString
                    MyOutputs.Surge = ""
                Case "Surge"
                    MyOutputs.Roll = ""
                    MyOutputs.Pitch = ""
                    MyOutputs.Heave = ""
                    MyOutputs.Yaw = ""
                    MyOutputs.Sway = ""
                    MyOutputs.Surge = cb_Slider.Value.ToString
            End Select
        Else
            'clear outputs
            MyOutputs.Roll = ""
            MyOutputs.Pitch = ""
            MyOutputs.Heave = ""
            MyOutputs.Yaw = ""
            MyOutputs.Sway = ""
            MyOutputs.Surge = ""

            'turn off
            tmr_Send.Enabled = False
        End If

        'send data
        SendData()
    End Sub

    'pause output for selection change
    Private Sub cbo_OutputSelector_DropDown(sender As Object, e As EventArgs) Handles cbo_OutputSelector.DropDown
        tmr_Send.Enabled = False
    End Sub

    'unpause output after selection change
    Private Sub cbo_OutputSelector_DropDownClosed(sender As Object, e As EventArgs) Handles cbo_OutputSelector.DropDownClosed
        tmr_Send.Enabled = True
    End Sub

    'show value
    Private Sub cb_Slider_ValueChanged(sender As Object, e As EventArgs) Handles cb_Slider.ValueChanged
        txt_ScrollValue.Text = cb_Slider.Value.ToString
    End Sub

    Private Function CheckController() As String


    End Function

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Dim PlayerIndex As UserIndex
        PlayerIndex = UserIndex.One
        If Not New Controller(PlayerIndex).IsConnected Then Console.WriteLine("Disconnected") ' GamePad is not connected.

        Dim GPS As Gamepad = New Controller(PlayerIndex).GetState.Gamepad ' GamePad State

        ' Registers a whole bunch of data about the current state of the controller.
        'Dim Str As String = $"Left Thumb: {GPS.LeftThumbX}, {GPS.LeftThumbY}{vbNewLine}" &
        '$"Right Thumb: {GPS.RightThumbX}, {GPS.RightThumbY}{vbNewLine}" &
        '$"LTrigger: {GPS.LeftTrigger} RTrigger: {GPS.RightTrigger}{vbNewLine}" &
        '$"Buttons Currently Pressed: {GPS.Buttons.ToString}"
        Dim val As Integer = GPS.RightThumbX
        If val > 32767 Then
            val = 32767
        ElseIf val < -32767 Then
            val = -32767
        End If
        cb_Slider.Value = val
        'Console.WriteLine(Str)
        ' Checks if the A button is pressed.
        'If GPS.Buttons.HasFlag(GPS.Buttons.A) Then
        ' Do something when A is pressed.
        'Else
        ' Do something when A is not pressed.
        'End If
        ' Copy that if-block as many times as needed to cover all the buttons used in the game.

    End Sub
End Class
