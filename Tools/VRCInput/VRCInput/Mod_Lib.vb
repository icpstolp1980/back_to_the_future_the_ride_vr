﻿Option Strict On
Option Explicit On

Imports System.Xml.Serialization
Imports System.IO
Imports System.Text
Imports System.Net.Sockets

Public Module Mod_Lib
    Private Client As New UdpClient
    Private bytCommand As Byte() = New Byte() {}

    'output structure
    Public MyOutputs As New MyDataInputs
    Public Structure MyDataInputs
        Private _Roll As String
        Private _Pitch As String
        Private _Heave As String
        Private _Yaw As String
        Private _Sway As String
        Private _Surge As String

        Public Property Roll() As String
            Get
                Return _Roll
            End Get
            Set(value As String)
                _Roll = value
            End Set
        End Property

        Public Property Pitch() As String
            Get
                Return _Pitch
            End Get
            Set(value As String)
                _Pitch = value
            End Set
        End Property

        Public Property Heave() As String
            Get
                Return _Heave
            End Get
            Set(value As String)
                _Heave = value
            End Set
        End Property

        Public Property Yaw() As String
            Get
                Return _Yaw
            End Get
            Set(value As String)
                _Yaw = value
            End Set
        End Property

        Public Property Sway() As String
            Get
                Return _Sway
            End Get
            Set(value As String)
                _Sway = value
            End Set
        End Property

        Public Property Surge() As String
            Get
                Return _Surge
            End Get
            Set(value As String)
                _Surge = value
            End Set
        End Property
    End Structure

    'send data
    Public Sub SendData()
        Dim xml_serializer As New XmlSerializer(GetType(MyDataInputs))
        Dim string_writer As New StringWriter
        xml_serializer.Serialize(string_writer, MyOutputs)
        Dim SerializedOutput As String = string_writer.ToString()
        string_writer.Close()
        bytCommand = Encoding.ASCII.GetBytes(SerializedOutput)
        Client.Send(bytCommand, bytCommand.Length)
    End Sub

    'start udp
    Public Sub StartUDP()
        Client.Connect("127.0.0.1", 5555)
    End Sub

    'stop udp
    Public Sub StopUDP()
        Client.Close()
    End Sub
End Module
