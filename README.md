[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](./LICENSE)

[![DUB](https://img.shields.io/github/tag/franck-talbart/back_to_the_future_the_ride_vr.svg)](https://github.com/franck-talbart/back_to_the_future_the_ride_vr)
=======

# Back to the Future: The Ride VR (BTTFTRVR)
![BTTFTRVR](Misc/pics/Back_to_the_Future_The_Ride_logo.png)

The biggest part of the work is done now.
This new release now support 2DOF and 3DOF simulators!

## Introduction

Back to the Future: The Ride (BTTFTR) was a simulator ride at Universal Studios theme parks, located at Universal Studios Florida and Universal Studios Hollywood.
The simulator was based on the BTTF movies. Sadly, it was permanently closed down in 2007. 

This project is an attempt to make the ride back to life in the most realistic possible way. Let's call it BTTFTRVR (Back To The Future: The Ride Virtual Reality).

## Demo

Click on the thumbnail to play it:

[![Alt text](https://img.youtube.com/vi/kINSU0sgc3c/0.jpg)](https://www.youtube.com/watch?v=kINSU0sgc3c)

Complete Walkthrough with preshow:

[![Alt text](https://img.youtube.com/vi/t2OzJFJKEDw/0.jpg)](https://www.youtube.com/watch?v=t2OzJFJKEDw)

## Support

Happy with the project? You can buy me a coffee!
https://ko-fi.com/franckarts
Thanks!

## How it works

The original footage was restored and remastered and is available online. This project provides a virtual reality environment to enjoy it (where the movie theater is reproduced), using Unity and an Oculus Rift or HTC Vive.
On top of that, a home made simulator along with a fan is used to add some magic. This project is compatible with SimTools to
support a 3DOF motion simulator and a fan.

## Compilation

Dependencies:

* git
* Blender (mandatory to open the project!)
* Unity (tested with 2018 LTS release, I have VR issues with the 2019 release)
* [Optional] SimTools to add a motion simulator
* [Optional] Motion Cancellation if a motion simulator is used (https://github.com/openvrmc/OpenVR-MotionCompensation)

OpenVR is a requirement (do not use the Oculus API) to be able to run a motion cancellation tool for your motion simulator.

First of all, download the missing video and audio files on http://franck.talbart.free.fr/bttftrvr/ and put them in Assets\BigAssets. ("deploy.bat" script)

### [Optional] Motion simulator

#### Arduino

The firmware used for the Arduino board can be found in Misc/arduino.

#### Plugin 

The SimTools plugin must be built and imported into the Simtools game manager. To create the plugin, go into Tools/Simtools Plugin/BTTFTRVRSimTools*/SimToolsGamePlugin/GamePlugin.
Generate the solution, and create a zip file containing the dll file as well as the MaxMin file from the bin dir.
This file can be imported into Simtools.

#### Gamedash

The fan is controller by Gamedash. The first dash gives the speed of the vehicle. The associated formula is the following:
```
MATH * 2
ROUND 0
PAD 3 0
```

Interface output:
```
S<Dash1>C
```

#### Simtools Game Engine

```
Output - bit range: 10
Output - type: Binary
BitsPerSec: 9600
DataBits: 8
Parity: None
Stop bits: 1
```

The Interface output is:
```
X1<Axis1a>CX2<Axis2a>CX3<Axis3a>CX4<Axis4a>CX5<Axis5a>CX6<Axis6a>C
```

The Shutdown output is:
```
XE00C
```

10ms

### Build 

To build the project, open it with Unity 2018. The produced bin must be called BTTFTRVR.exe. This is important
as the process name is used by SimTools to recognise the game.
SimTools is not mandatory to enjoy this project. A simple VR headset is enough. However,
the telemetry part (3DOF motion simulator + fan) add a lot of fun :) 

## Nota Bene

At the beginning of the ride, please check the motion cancellation is still working.
For some reasons which need to be investigated, the cancellation may be disabled while starting the app.
Also, please disable the openVR Input Simulator if installed.
This ride currently supports 3 axes, please do not enable the others (it would reduce the effects):
DOF1 => Roll
DOF2 => Pitch
DOF3 => Heave

## Licence

The BTTFTRVR is released under the GNU General Public License, version 3. Please refer to [LICENSE](./LICENSE), for details.

## Authors

The author list is written in the [AUTHORS](./AUTHORS) file.

## Contact

franck@talbart.fr
